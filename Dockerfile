FROM ubuntu:latest
MAINTAINER Michael Dodwell <michael@dodwell.us>

ENV DEBIAN_FRONTEND noninteractive

# Install pre-reqs for external repos
RUN apt-get update -qqy \
  && apt-get -qqy --no-install-recommends install \
     software-properties-common

# Update software list, install Oracle Java
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
  && add-apt-repository -y ppa:webupd8team/java \
  && apt-get update -qqy \
  && apt-get -qqy --no-install-recommends install \
     oracle-java8-installer \
     oracle-java8-set-default \
     bsdtar \
     wget \
     curl \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/* /var/cache/oracle-jdk8-installer

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

# Install MMC
RUN mkdir -p /opt/mmc && \
    wget --no-check-certificate -q https://s3.amazonaws.com/MuleEE/mmc-distribution-mule-console-bundle-3.7.0.zip -O /tmp/mmc.zip && \ 
    /usr/bin/bsdtar -xf /tmp/mmc.zip -s'|[^/]*/||' -C /opt/mmc && \
    rm -f /tmp/mmc.zip

EXPOSE 7777
EXPOSE 8585

COPY build/daemon-wrapper.sh /opt/mmc/daemon-wrapper.sh
CMD ["/opt/mmc/daemon-wrapper.sh"]
