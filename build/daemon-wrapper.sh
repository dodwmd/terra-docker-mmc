#! /usr/bin/env bash
set -eu

# Proxy signals
function kill_app(){
    kill $(cat $pidfile)
    exit 0 # exit okay
}
trap "kill_app" SIGINT SIGTERM

# Launch daemon
cd /opt/mmc
./startup.sh -p 7777
sleep 2

mmc_pid=$(pgrep -f /opt/mmc/mmc-3.7.0/apache-tomcat-7.0.52)
mule_pid=$(pgrep -f /opt/mmc/mule-enterprise-3.7.0/lib/boot/exec)

# Loop while the tomcat and mmc processes exist
while kill -0 ${mmc_pid} && kill -0 ${mule_pid} ; do
    sleep 0.5
done
exit 1000 # exit unexpected
